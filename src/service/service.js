function convertToMinutes(time) {
    let total = time / 60;
    let minutes = 0, seconds = '00';
    let new_seconds = time % 60;
    if(total > 1) {
        minutes = total;
        seconds = new_seconds;
    } else {
        minutes = 0;
        seconds = new_seconds;
    }
    var new_str = '';
    if(seconds.toString().length < 2) {
        new_str = "0"+Math.floor(minutes)+":0"+seconds;
    } else {
        new_str = "0"+Math.floor(minutes)+":"+seconds;
    }
    return new_str;
}

function shuffleArray(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}


export {shuffleArray, convertToMinutes}