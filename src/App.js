import React from 'react';
import './App.css';
import ActualPage from './components/ActualPage';
import Header from './components/header';

export default class App extends React.Component {
  state = {
    formSubmitted: false
  }
  handleStartGame = () => {
    this.setState({
      formSubmitted: true
    });
  }
  render() {
    let {formSubmitted} = this.state;
    return (
      <div className="App">
        { (formSubmitted) ? 
          <ActualPage/> :         
          <div style={{clear: 'both', textAlign: 'center'}}>
            <div>
              <div>
                <h3>
                  Play & win upto 100% discount <span style={{color: '#FF2E01'}}>in seconds!</span>
                </h3>
                <div>Just tell your concerns & steal your deal</div>
                <div style={{marginTop: '10px'}}>
                  <button onClick={this.handleStartGame} style={{backgroundColor: '#6BBD59', padding: '10px 20px',border: 'none', color: 'white', borderRadius: '5px' }}>PLAY NOW</button>
                </div>
                <hr/>
                <div style={{padding: '20px'}}>
                  <iframe width="420" height="315" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>
                </div>
                <div style={{border: '1px solid #80808040', display: 'inline-block', borderRadius: '3px', padding: '2px'}}>
                  <span style={{backgroundColor: 'black', borderRadius: '5px', color: 'white', margin: '0px 1px', padding: '2px'}}>9</span>
                  <span style={{backgroundColor: 'black', borderRadius: '5px', color: 'white', margin: '0px 1px', padding: '2px'}}>9</span> 
                  <span>Players won this deal for free</span>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}
