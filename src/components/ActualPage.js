import React from 'react';
import Concerns from './Concerns';
import UserForm from './userForm';
import Draggable from 'react-draggable';
import {shuffleArray} from '../service/service';
import CartImage from '../assets/cart-icon.svg';

const productList =[
    {
        id: '1',
        title: 'Titles',
        image: 'https://cdn.shopify.com/s/files/1/2393/2199/products/1_protein_and_herbs_x280@2x.jpg'
    },
    {
        id: '2',
        title: 'Other Gummies',
        image: 'https://cdn.shopify.com/s/files/1/2393/2199/products/1_protein_and_herbs_x280@2x.jpg'
    },
    {
        id: '3',
        title: 'Gummies',
        image: 'https://cdn.shopify.com/s/files/1/2393/2199/products/1_protein_and_herbs_x280@2x.jpg'
    },
    {
        id: '4',
        title: 'Syrup',
        image: 'https://cdn.shopify.com/s/files/1/2393/2199/products/1_protein_and_herbs_x280@2x.jpg'
    },
    {
        id: '5',
        title: 'Face wash',
        image: 'https://cdn.shopify.com/s/files/1/2393/2199/products/1_protein_and_herbs_x280@2x.jpg'
    },
    {
        id: '6',
        title: 'Collagen Builder',
        image: 'https://cdn.shopify.com/s/files/1/2393/2199/products/1_protein_and_herbs_x280@2x.jpg'
    },
    {
        id: '7',
        title: 'Protein & Herbs',
        image: 'https://cdn.shopify.com/s/files/1/2393/2199/products/1_protein_and_herbs_x280@2x.jpg'
    }
]

export default class ActualPage extends React.Component {
    state = {
        activeDrags: 0,
        deltaPosition: {
            x: 0, y: 0
        },
        cartList: [],
        products: productList
    };
    handleDrag = (e, ui) => {
        const {x, y} = this.state.deltaPosition;
        this.setState({
          deltaPosition: {
            x: x + ui.deltaX,
            y: y + ui.deltaY,
          }
        });
    };
    onStart = () => {
        let s = this.state.activeDrags;
        console.log(s);
        this.setState({activeDrags: ++s});
    };
    
    onStop = () => {
        let p = this.state.activeDrags
        this.setState({activeDrags: --p});
    };

    onDrop = (e) => {
        console.log(e.target);
        let q = this.state.activeDrags;
        this.setState({activeDrags: --q});
        if (e.target.classList.contains("drop-target")) {
          alert("Dropped!");
          e.target.classList.remove('hovered');
        }
    };
    handleConcern = () => {
        // Shuffle items
        let items = this.state.products;
        let new_arr = shuffleArray(items);
        console.log(new_arr);
        this.setState({
            products: new_arr
        });
    }
    render() {
        const dragHandlers = {onStart: this.onStart, onStop: this.onStop};
        let {cartList, products} = this.state;
        return(
            <div style={{marginTop: '20px'}}>
                <h3 style={{textAlign: 'center'}}>Choose your concerns</h3>
                {(true) ?
                    <div style={{display: 'flex', flexDirection: 'column', alignSelf: 'baseline', width: '100%', textAlign: 'center', height: '100vh'}}>
                        <Concerns selectedConcern={this.handleConcern}/>
                        <div className="container-holder">
                            <div>
                                {products.map((item, index) => {
                                    return(
                                        <Draggable defaultClassName="box-holder" key={index} {...dragHandlers} onDrop={this.onDrop}>
                                            <div className="box">
                                                <img alt="SomeImage" src={item.image} style={{width: '100%'}}/>
                                                <div>
                                                    {item.title}
                                                </div>
                                            </div>
                                        </Draggable>
                                    );
                                })}
                            </div>
                        </div>
                        <div style={{marginTop: '50px'}}>
                            <img src={CartImage} style={{width: '300px'}}/>
                            {cartList ? cartList.map((item, index) => {
                                return(
                                    <div className="box" key={index}>
                                        <img alt="SomeImage" src={item.image} style={{width: '100%'}}/>
                                        <div>
                                            {item.title}
                                        </div>
                                    </div>
                                );
                            }): null}
                        </div>
                    </div>
                : <UserForm handleStartGame={this.startStatus}/>}
            </div>
        );
    }
}