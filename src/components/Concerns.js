import React from 'react';
import Timer from './timer';
import PopUp from './PopUp';

export default class Concerns extends React.Component {
    state = {
        selected: 0,
        showPopUp: false
    };
    handleRadioSelection = (e, i) => {
        let old = this.state.selected;
        if(old === i) {
            return false;
        }
        this.setState({
            selected: i
        });
        this.props.selectedConcern('changed');
    }
    handlePopUpOpen = () => {
        this.setState({
            showPopUp: true
        });
        console.log('Show');
    }
    render() {
        let {selected, showPopUp} = this.state;
        console.log(selected);
        return(
            <div>
                <div className="concerns">
                    <div className="form-input-holder">
                        <div className={"concern-button " + (selected === 1 ? "selected" : "")} onClick={(e) => this.handleRadioSelection(e, 1)}>SKIN</div>
                    </div>
                    <div className="form-input-holder">
                        <div className={"concern-button " + (selected === 2 ? "selected" : "")} onClick={(e) => this.handleRadioSelection(e, 2)}>HAIR</div>
                    </div>
                    <div className="form-input-holder">
                        <div className={"concern-button " + (selected === 3 ? "selected" : "")} onClick={(e) => this.handleRadioSelection(e, 3)}>FITNESS</div>
                    </div>
                </div>
                <Timer showPopUp={this.handlePopUpOpen}/>
                {showPopUp ? <PopUp/> : null}
            </div>
        )
    }
}