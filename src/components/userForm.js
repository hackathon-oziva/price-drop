import React from 'react';


export default class UserForm extends React.Component {
    state = {
        fullName: '',
        mobile: ''
    };
    handleStartGame = () => {
        console.log('Some');
        this.props.handleStartGame(true);
    }
    render() {
        let { fullName, mobile } = this.state;
        return(
            <div className="user-form">
                <div className="form-input-holder">
                    <label>Full Name</label>
                    <input type="text" value={fullName} onChange={(e) => this.setState({fullName: e.target.value})} placeholder="Enter Full Name"/>
                </div>
                <div className="form-input-holder">
                    <label>Enter Contact</label>
                    <input type="text" value={mobile} onChange={(e) => this.setState({mobile: e.target.value})} placeholder="Enter Mobile Number"/>
                </div>
                <div style={{clear: 'both', textAlign: 'center'}}>
                    <button onClick={this.handleStartGame}>Share and Avail Offer</button>
                </div>
            </div>
        );
    }
}