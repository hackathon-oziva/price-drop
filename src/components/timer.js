import React from 'react';
import {convertToMinutes} from '../service/service'

export default class Timer extends React.Component {
    state = {
        timer: 120,
    }
    updateTimer = () => {
        let {timer} = this.state;
        let props = this.props;
        let cls = setInterval(() => {
            if(timer === 1) {
                props.showPopUp();
                clearInterval(cls);
            }
            timer--;
            this.setState({timer});
        }, 1000);
    }
    componentDidMount() {
        this.updateTimer();
    }
    render() {
        let { timer } = this.state;
        return(
            <div style={{padding: '10px', fontSize: 24, float: 'right'}}>
                <span>{convertToMinutes(timer)}</span>
            </div>
        );
    }
}