import React from 'react';
import Happy from '../assets/happy.png';

export default class PopUp extends React.Component {
    render() {
        return(
            <div className="overlay">
                <div className="success-box">
                    <div>
                        <img src={Happy} style={{width: '100px'}}/>
                    </div>
                    <div>
                        <h3 style={{color: 'rgb(231,76,43)'}}>Yayyyy!</h3>
                        <h5>You won 50% off on these products</h5>
                    </div>
                    <div>
                        <img/>
                        <img/>
                        <img/>
                        <img/>
                    </div>
                    <div>
                        <button className="action-button back-green">SHARE TO PLAY AGAIN</button>
                        <button className="action-button green">BUY NOW</button>
                    </div>
                </div>
            </div>
        );
    }
}